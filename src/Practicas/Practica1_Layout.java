package Practicas;

import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

import java.awt.FlowLayout;

public class Practica1_Layout extends JFrame {

    JButton botonCerrar;
    JTextField Nombre;

    public Practica1_Layout() {
        this.setSize(640, 480);
        this.setTitle("Practica 1 (Layout)");
        this.setLayout(new FlowLayout());

        // Con la siguiente linea, ya no es necesario implementar WindowListener
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //---

        botonCerrar = new JButton("Cerrar");
        botonCerrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Desde metodo en clase anonima");
                System.exit(0);
            }
        });

        Nombre = new JTextField(30);

        this.add(Nombre);
        this.add(botonCerrar);

        this.setVisible(true);
    }
    
         public static void main(String args[]){         
         Practica1_Layout practica1 = new Practica1_Layout();         
     }
}

class MiListener extends WindowAdapter implements ActionListener {

    void cerrar(){
        System.exit(0);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
            System.out.println("Se presionó el boton cerrar");
            cerrar();
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Cerrandose la ventana");
        cerrar();
    }
}