
package Practicas;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.*;


public class Practica1 extends JFrame{
    
    JButton btnCerrar;
    
    Practica1(){
        this.setSize(300, 200);
        this.setTitle("Practica 1");
        btnCerrar=new JButton("Cerrar");
        MiActionListener ml = new MiActionListener();
        btnCerrar.addActionListener(ml);
        this.add(btnCerrar);
        
    }
    
    public static void main (String args[]){
        Practica1 p=new Practica1();
        p.setVisible(true);
    }
}

class MiActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent arg0) {
       System.exit(0);
    }
    
}
