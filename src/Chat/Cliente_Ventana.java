package Chat;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import test.Cliente; 
import test.Servidor;


public class Cliente_Ventana extends javax.swing.JFrame implements Runnable {
    
    

    DefaultListModel listaUsuarios = new DefaultListModel();
    BufferedReader in;
    PrintWriter out;
    Socket cnx;

    public Cliente_Ventana() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        listaUsuarios = new DefaultListModel();
        JlistUsuarios.setModel(listaUsuarios);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        label_mensaje = new javax.swing.JLabel();
        label_usuario = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        chat = new javax.swing.JTextArea();
        mensaje = new javax.swing.JTextField();
        enviar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JlistUsuarios = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        label_mensaje.setFont(new java.awt.Font("Segoe Script", 1, 48)); // NOI18N
        label_mensaje.setForeground(new java.awt.Color(255, 255, 0));
        label_mensaje.setText("Mensajes");
        jPanel1.add(label_mensaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 40, -1, -1));

        label_usuario.setFont(new java.awt.Font("Segoe Script", 1, 48)); // NOI18N
        label_usuario.setForeground(new java.awt.Color(255, 255, 0));
        label_usuario.setText("Usuarios");
        jPanel1.add(label_usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 40, -1, -1));

        chat.setEditable(false);
        chat.setColumns(20);
        chat.setFont(new java.awt.Font("Trebuchet MS", 0, 20)); // NOI18N
        chat.setRows(5);
        jScrollPane3.setViewportView(chat);

        jPanel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 440, 310));

        mensaje.setFont(new java.awt.Font("Trebuchet MS", 1, 24)); // NOI18N
        mensaje.setText("Mensaje");
        mensaje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mensajeMouseClicked(evt);
            }
        });
        mensaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mensajeActionPerformed(evt);
            }
        });
        jPanel1.add(mensaje, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 450, 440, 70));

        enviar.setBackground(new java.awt.Color(255, 255, 255));
        enviar.setIcon(new javax.swing.ImageIcon("C:\\Users\\aleja\\Desktop\\Materias\\1-Topicos Avanzados de Programación\\Iconos\\btn enviar.jpg")); // NOI18N
        enviar.setBorder(null);
        enviar.setOpaque(false);
        enviar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                enviarMouseClicked(evt);
            }
        });
        enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enviarActionPerformed(evt);
            }
        });
        jPanel1.add(enviar, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 430, 130, 100));

        JlistUsuarios.setFont(new java.awt.Font("Trebuchet MS", 0, 20)); // NOI18N
        jScrollPane1.setViewportView(JlistUsuarios);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 120, 170, 300));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\aleja\\Desktop\\Materias\\1-Topicos Avanzados de Programación\\Iconos\\fondo mensajes.jpg")); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 740, 120));

        fondo.setIcon(new javax.swing.ImageIcon("C:\\Users\\aleja\\Desktop\\Materias\\1-Topicos Avanzados de Programación\\Iconos\\fondo.jpg")); // NOI18N
        jPanel1.add(fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 740, 560));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void enviarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_enviarMouseClicked

    }//GEN-LAST:event_enviarMouseClicked

    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enviarActionPerformed
        String mensaje = this.mensaje.getText();
        this.send(mensaje);
        this.mensaje.setText("");
        chat.setText(chat.getText() + " " + mensaje + "\n");
    }//GEN-LAST:event_enviarActionPerformed

    private void mensajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mensajeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mensajeActionPerformed

    private void mensajeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mensajeMouseClicked
        mensaje.setText("");
    }//GEN-LAST:event_mensajeMouseClicked

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cliente_Ventana().run();
            }
        });

       
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> JlistUsuarios;
    private javax.swing.JTextArea chat;
    private javax.swing.JButton enviar;
    private javax.swing.JLabel fondo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel label_mensaje;
    private javax.swing.JLabel label_usuario;
    private javax.swing.JTextField mensaje;
    // End of variables declaration//GEN-END:variables

    void start() {
        ConexionCliente hilo;
        try {
            this.cnx = new Socket("192.168.100.131", 4444);
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(), true);

            hilo = new ConexionCliente(in);
            hilo.start();

        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void send(String mensaje) {
        out.println(mensaje);
    }

    @Override
    public void run() {
        start();
        this.setVisible(true);
    }

    
    
    
    
    class ConexionCliente extends Thread {
        
        public boolean ejecutar = true;
        BufferedReader in;

        public ConexionCliente(BufferedReader in) {
            this.in = in;
        }
        
        
        
        @Override
        public void run() {
            String respuesta = "";
            boolean login = false;
            String user = null;
            while (ejecutar) {
                try {
                    respuesta = in.readLine();
                    if (respuesta != null) {

                        while (!login) {
                            if (respuesta.equals("Bienvenido, proporcione su usuario")) {
                                user = JOptionPane.showInputDialog("Ingrese su nombre");
                                out.println(user);

                                                         
                                
                                if (null != user) {
                                    switch (user) {
                                        case "hugo":
                                            listaUsuarios.addElement("hugo");
                                            break;
                                        case "luis":
                                            listaUsuarios.addElement("luis");
                                            break;
                                        case "paco":
                                            listaUsuarios.addElement("paco");
                                            break;
                                        case "donald":
                                            listaUsuarios.addElement("donald");
                                            break;
                                        case "jose":
                                            listaUsuarios.addElement("jose");
                                            break;
                                        default:
                                            break;
                                    }
                                }

                            } else if (respuesta.equals("Escriba el password")) {
                                String pass = JOptionPane.showInputDialog("Ingrese su contraseña");
                                out.println(pass);
                            } else {
                                login = !login;
                            }
                            respuesta = in.readLine();
                        }
                        chat.setText(chat.getText() + respuesta + "\n");

                        if (respuesta.contains(" Se ha unido a la conversacion.")) {
                            listaUsuarios.addElement(respuesta.substring(0, 4));
                        } else if (respuesta.contains("ha salido del chat.")) {
                            listaUsuarios.removeElement(respuesta.substring(0, 4));
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
        
        
        
    }

}
