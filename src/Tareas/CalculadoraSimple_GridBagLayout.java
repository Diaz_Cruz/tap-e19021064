/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tareas;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author aleja
 */
public class CalculadoraSimple_GridBagLayout extends JFrame {

    JButton pantalla;

    CalculadoraSimple_GridBagLayout() {
        this.setSize(300, 350);
        this.setTitle("Calculadora");
        this.setLayout(new BorderLayout(10, 10));
        pantalla = new JButton("0");
        pantalla.setFont(new Font("Arial", Font.PLAIN, 30));
        pantalla.setEnabled(false);
        BotonesCalculadora b = new BotonesCalculadora();

        //Cerrar el programa 
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.add(b);
        this.add(pantalla, BorderLayout.NORTH);
        this.setVisible(true);
    }

    public static void main(String args[]) {
        CalculadoraSimple_GridBagLayout cs = new CalculadoraSimple_GridBagLayout();
    }

}

class BotonesCalculadora extends JPanel {

    GridBagConstraints btn = new GridBagConstraints();

    public BotonesCalculadora() {

        this.setLayout(new GridBagLayout());

        JButton boton7 = new JButton("7");
        boton7.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 1;
        btn.gridy = 0;
        btn.weightx = 1.0;
        btn.weighty = 1.0;
        add(boton7, btn);

        JButton boton8 = new JButton("8");
        boton8.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 2;
        btn.gridy = 0;
        add(boton8, btn);

        JButton boton9 = new JButton("9");
        boton9.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 3;
        btn.gridy = 0;
        add(boton9, btn);

        JButton boton4 = new JButton("4");
        boton4.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 1;
        btn.gridy = 1;
        add(boton4, btn);

        JButton boton5 = new JButton("5");
        boton5.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 2;
        btn.gridy = 1;
        add(boton5, btn);

        JButton boton6 = new JButton("6");
        boton6.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 3;
        btn.gridy = 1;
        add(boton6, btn);

        JButton boton1 = new JButton("1");
        boton1.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 1;
        btn.gridy = 2;
        add(boton1, btn);

        JButton boton2 = new JButton("2");
        boton2.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 2;
        btn.gridy = 2;
        add(boton2, btn);

        JButton boton3 = new JButton("3");
        boton3.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 3;
        btn.gridy = 2;
        add(boton3, btn);

        JButton botonIgual = new JButton("=");
        botonIgual.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 1;
        btn.gridy = 3;
        add(botonIgual, btn);

        JButton boton0 = new JButton("0");
        boton0.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 2;
        btn.gridy = 3;
        add(boton0, btn);

        JButton botonMas = new JButton("+");
        botonMas.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 4;
        btn.gridy = 3;
        add(botonMas, btn);

        JButton botonPunto = new JButton(".");
        botonPunto.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 3;
        btn.gridy = 3;
        btn.ipadx = 10;
        add(botonPunto, btn);

        JButton botonMenos = new JButton("-");
        botonMenos.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 4;
        btn.gridy = 2;
        add(botonMenos, btn);

        JButton botonDivision = new JButton("/");
        botonDivision.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 4;
        btn.gridy = 0;
        add(botonDivision, btn);

        JButton botonPor = new JButton("*");
        botonPor.setFont(new Font("Arial", Font.PLAIN, 40));
        btn.gridx = 4;
        btn.gridy = 1;
        btn.ipadx = 6;
        add(botonPor, btn);

    }

}
