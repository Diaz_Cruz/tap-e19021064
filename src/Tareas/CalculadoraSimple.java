package Tareas;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

public class CalculadoraSimple extends JFrame {

    JButton pantalla;

    CalculadoraSimple() {
        this.setSize(400, 450);
        this.setTitle("Calculadora");
        this.setLayout(new BorderLayout(15, 15));
        pantalla = new JButton("0");
        pantalla.setPreferredSize(new Dimension(100, 60));
        pantalla.setEnabled(false);

        BotonesCalculadora b = new BotonesCalculadora();
        //Cerrar el programa 
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.add(b, BorderLayout.CENTER);
        this.add(pantalla, BorderLayout.NORTH);
        this.setVisible(true);
    }

    public static void main(String args[]) {
        CalculadoraSimple cs = new CalculadoraSimple();
    }
}

class BotonesCalculadora extends JPanel {

    public BotonesCalculadora() {
        setLayout(new BorderLayout());
        setLayout(new GridLayout(4, 4));
        //7-8-9-/
        JButton boton7 = new JButton("7");
        add(boton7);
        JButton boton8 = new JButton("8");
        add(boton8);
        JButton boton9 = new JButton("9");
        add(boton9);
        JButton botonDivision = new JButton("/");
        add(botonDivision);

        //4-5-6-*
        JButton boton4 = new JButton("4");
        add(boton4);
        JButton boton5 = new JButton("5");
        add(boton5);
        JButton boton6 = new JButton("6");
        add(boton6);
        JButton botonPor = new JButton("*");
        add(botonPor);

        //1-2-3--
        JButton boton1 = new JButton("1");
        add(boton1);
        JButton boton2 = new JButton("2");
        add(boton2);
        JButton boton3 = new JButton("3");
        add(boton3);
        JButton botonMenos = new JButton("-");
        add(botonMenos);

        //=-0-.-+
        JButton botonIgual = new JButton("=");
        add(botonIgual);
        JButton boton0 = new JButton("0");
        add(boton0);
        JButton botonPunto = new JButton(".");
        add(botonPunto);
        JButton botonMas = new JButton("+");
        add(botonMas);
    }

}
