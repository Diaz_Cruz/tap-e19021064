/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos.reloj;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author ale
 */
public class Crono extends Thread {

    JLabel eti;

    public Crono(JLabel cronometro) {
        this.eti = cronometro;
    }

    @Override
    public void run() {

        try {

            int x = 0;
            while (Cronometro.iniciaHilo == true) {
                Thread.sleep(1000);
                ejecutarHiloCronometro(x);
                x++;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Cronometro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ejecutarHiloCronometro(int x) {
        Cronometro.Csegundos++;
        if (Cronometro.Csegundos > 59) {
            Cronometro.Csegundos = 0;
            Cronometro.Cminutos++;
            if (Cronometro.Cminutos > 59) {
                Cronometro.Cminutos = 0;
                Cronometro.Chora++;
            }
        }
        String textSeg = "", textMin = "", textHora = "";

        if (Cronometro.Csegundos < 10) {
            textSeg = "0" + Cronometro.Csegundos;
        } else {
            textSeg = "" + Cronometro.Csegundos;
        }
        if (Cronometro.Cminutos < 10) {
            textMin = "0" + Cronometro.Cminutos;
        } else {
            textMin = "" + Cronometro.Cminutos;
        }
        if (Cronometro.Chora < 10) {
            textHora = "0" + Cronometro.Chora;
        } else {
            textHora = "" + Cronometro.Chora;
        }

        String reloj = textHora += ":" + textMin + ":" + textSeg;
        eti.setText(reloj);

    }

}
